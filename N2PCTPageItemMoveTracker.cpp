//========================================================================================
//  
//  $File: //depot/shuksan/source/sdksamples/Resizeshot/MyMovePageItemTracker.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: rgano $
//  
//  $DateTime: 2005/01/09 20:46:10 $
//  
//  $Revision: #9 $
//  
//  $Change: 309245 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

//Interface includes:
#include "IEvent.h"
#include "IControlView.h"
#include "ILayoutControlViewHelper.h"
#include "ILayoutControlData.h"
#include "ILayoutCmdData.h"
#include "IDatabase.h"
#include "IDocument.h"
#include "ISpread.h"
#include "IPanorama.h"
#include "IZoomCmdData.h"
#include "IWorkspace.h"
#include "IK2ServiceRegistry.h"
#include "IK2ServiceProvider.h"
#include "IImageWriteFormatInfo.h"
#include "ILayoutSelectionSuite.h"
#include "ISelectionManager.h"
#include "ISelectionUtils.h"

// General includes
#include "SDKFileHelper.h"
#include "JPEGFilterID.h"
#include "SplineID.h"
#include "ShuksanID.h"
#include "LayoutUIID.h"
#include "ILayoutUIUtils.h"
#include "IPathUtils.h"
#include "IPageItemUtils.h"
#include "PersistUtils.h"
#include "CAlert.h"
#include "CmdUtils.h"
#include "TRACE.h"
#include "SelectUtils.h"
#include "ImageID.h"
#include "GIFFilterID.h"
#include "CTracker.h"
#include "JPEGFilterID.h"
#include "TIFFFilterID.h"
#include "StreamUtil.h"

// Project includes
#include "N2PFrOvtID.h"



/**
	The Resizeshot Tool's tracker. Resizeshot creates images of spreads, pages
	and selections.

	The user must double click to take the Resizeshot.

	If a selection is set double clicking takes a Resizeshot of it.

	If no selection is set double clicking on the background of the spread creates a
	Resizeshot of the spread. Double clicking on a page creates a Resizeshot of the page.

	By selecting a frame with no fill or stroke you can create Resizeshots clipped to the
	bounds of this frame. Use the selection tool to drag the frame around and create
	Resizeshots of different areas.

	The tool has global preferences that allow the name of the output file, the file
	format, scale factor and various other parameters to be set.

	If the output file specified by the preferences is an absolute filenname it will
	be used. Otherwise the Resizeshot image file is created in the same folder as the 
	document from which Resizeshots are being made. 

 	This tracker completes all of its behaviour in a single call to BeginTracking().

	
	@ingroup Resizeshot
*/
class MyMovePageItemTracker : public CTracker
{
	public:
		/**The constructor */ 
		MyMovePageItemTracker(IPMUnknown* boss);

		/** Destructor*/
		~MyMovePageItemTracker() {}

		/**
			 Invoked when the tool is selected and the user clicks the mouse. 
		*/
		virtual bool16 BeginTracking(IEvent* theEvent);
		
		
		virtual bool16 EndTracking(IEvent* theEvent);
		
};




/*
 CREATE_PMINTERFACE
 This macro creates a class factory for the given class name
 and registers the ID with the host.
*/
CREATE_PMINTERFACE(MyMovePageItemTracker, kMyMovePageItemTrackerImpl)


/*
 Constructor for MyMovePageItemTracker class.
*/
MyMovePageItemTracker::MyMovePageItemTracker(IPMUnknown* boss) :
	CTracker(boss)
{
	TRACEFLOW("SnapShot","SnapTracker constructor. \n");
}


/*
 Begin tracking, invoked when the tool is selected and the user clicks the mouse. 
*/
bool16 MyMovePageItemTracker::BeginTracking(IEvent* theEvent)
{
	return kTrue;
}

bool16 MyMovePageItemTracker::EndTracking(IEvent* theEvent)
{
	return kTrue;
}