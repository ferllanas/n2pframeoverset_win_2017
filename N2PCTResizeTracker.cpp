//========================================================================================
//  
//  $File: //depot/shuksan/source/sdksamples/Resizeshot/ResizeTracker.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: rgano $
//  
//  $DateTime: 2005/01/09 20:46:10 $
//  
//  $Revision: #9 $
//  
//  $Change: 309245 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

//Interface includes:
#include "IEvent.h"
#include "IControlView.h"
#include "ILayoutControlViewHelper.h"
#include "ILayoutControlData.h"
#include "ILayoutCmdData.h"
#include "IDatabase.h"
#include "IDocument.h"
#include "ISpread.h"
#include "IPanorama.h"
#include "IZoomCmdData.h"
#include "IWorkspace.h"
#include "IK2ServiceRegistry.h"
#include "IK2ServiceProvider.h"
#include "IImageWriteFormatInfo.h"
#include "ILayoutSelectionSuite.h"
#include "ISelectionManager.h"
#include "ISelectionUtils.h"
#include "IGraphicFrameData.h"

// General includes
#include "SDKFileHelper.h"
#include "JPEGFilterID.h"
#include "SplineID.h"
#include "ShuksanID.h"
#include "LayoutUIID.h"
#include "ILayoutUIUtils.h"
#include "IPathUtils.h"
#include "IPageItemUtils.h"
#include "PersistUtils.h"
#include "CAlert.h"
#include "CmdUtils.h"
#include "TRACE.h"
#include "SelectUtils.h"
#include "ImageID.h"
#include "GIFFilterID.h"
#include "CTracker.h"
#include "JPEGFilterID.h"
#include "TIFFFilterID.h"
#include "StreamUtil.h"

// Project includes
#include "N2PFrOvtID.h"
//#include "IN2PCTUtilities.h"



/**
	The Resizeshot Tool's tracker. Resizeshot creates images of spreads, pages
	and selections.

	The user must double click to take the Resizeshot.

	If a selection is set double clicking takes a Resizeshot of it.

	If no selection is set double clicking on the background of the spread creates a
	Resizeshot of the spread. Double clicking on a page creates a Resizeshot of the page.

	By selecting a frame with no fill or stroke you can create Resizeshots clipped to the
	bounds of this frame. Use the selection tool to drag the frame around and create
	Resizeshots of different areas.

	The tool has global preferences that allow the name of the output file, the file
	format, scale factor and various other parameters to be set.

	If the output file specified by the preferences is an absolute filenname it will
	be used. Otherwise the Resizeshot image file is created in the same folder as the 
	document from which Resizeshots are being made. 

 	This tracker completes all of its behaviour in a single call to BeginTracking().

	
	@ingroup Resizeshot
*/
class ResizeTracker : public CTracker
{
	public:
		/**The constructor */ 
		ResizeTracker(IPMUnknown* boss);

		/** Destructor*/
		~ResizeTracker() {}

		/**
			 Invoked when the tool is selected and the user clicks the mouse. 
		*/
		virtual bool16 BeginTracking(IEvent* theEvent);
	
	//private:
	
	//UID getUIDRefOfSelectionObserver();
};


/*
 CREATE_PMINTERFACE
 This macro creates a class factory for the given class name
 and registers the ID with the host.
*/
CREATE_PMINTERFACE(ResizeTracker, kResizeTrackerImpl)


/*
 Constructor for ResizeTracker class.
*/
ResizeTracker::ResizeTracker(IPMUnknown* boss) :
	CTracker(boss)
{
	TRACEFLOW("SnapShot","SnapTracker constructor. \n");
}


/*
 Begin tracking, invoked when the tool is selected and the user clicks the mouse. 
*/
bool16 ResizeTracker::BeginTracking(IEvent* theEvent)
{
	/* InterfacePtr<IN2PCTUtilities> N2PCTUtils(static_cast<IN2PCTUtilities*> (CreateObject
					(
						kN2PCTUtilitiesBoss,	// Object boss/class
						IN2PCTUtilities::kDefaultIID
					)));
					
		int32 NumTxtFmOverset=N2PCTUtils->LengthXMP("N2PTxtFmeOversetArray");
 		StructOfTextFrameConOverset *ListTextFrOrSt = new StructOfTextFrameConOverset[NumTxtFmOverset]; 
		N2PCTUtils->ObtenerTextFrameOrStOnXMP(ListTextFrOrSt,NumTxtFmOverset);
 		
 		bool16 EsUnOversetFrame=kTrue;
 		
 		UID UIDFrameSelected = this->getUIDRefOfSelectionObserver();
 		int32 intUIFrame = UIDFrameSelected.Get();
 		intUIFrame=intUIFrame-1;
 		for(int32 i=0 ;i<NumTxtFmOverset && EsUnOversetFrame==kFalse;i++)
 		{
 			PMString kk="";
 			kk.AppendNumber(ListTextFrOrSt[i].UIDTextFrameOrSt);
 			kk.Append(",");
 			kk.AppendNumber(intUIFrame);
 			
 			if(ListTextFrOrSt[i].UIDTextFrameOrSt==intUIFrame)
 			{
 				EsUnOversetFrame=kFalse;
 			}
 		}*/
 		
	return kFalse;
}


/*UID ResizeTracker::getUIDRefOfSelectionObserver()
{
	UID result = kInvalidUID;
	do
	{		
			IDocument* document = ::GetFrontDocument();
			if (document == nil)
			{
				break;
			}
			
			
			IDataBase* db = ::GetDataBase(document);
			if (db == nil)
			{
				ASSERT_FAIL("db is invalid");
				break;
			}
			
			Utils<ISelectionUtils> iSelectionUtils;
			if (iSelectionUtils == nil) 
			{
				break;
			}

		
			ISelectionManager* iSelectionManager = iSelectionUtils->GetActiveSelection();
			if(iSelectionManager==nil)
			{
				break;
			}

			//	Para textFrame 
			InterfacePtr<IConcreteSelection> pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kNewLayoutSelectionBoss)); // deprecated but universal (CS/2.0.2) 
			if(pTextSel==nil)
			{
				ASSERT_FAIL("pTextSel");
				break;
			}


			UIDRef selectedFrameRef = UIDRef::gNull;
			UID selectedFrameItem = kInvalidUID;
			InterfacePtr<ILayoutTarget> pLayoutTarget(pTextSel, UseDefaultIID());    
			if (pLayoutTarget)
			{
				UIDList itemList = pLayoutTarget->GetUIDList(kTrue);
				if (itemList.Length() == 1) 
				{
						// get the database
						db = itemList.GetDataBase();
						// get the UID of the selected item
						selectedFrameItem = itemList.At(0);

						selectedFrameRef=itemList.GetRef(0);
				}
			}
			else
			{
				ASSERT_FAIL("False");
				break;
			}

			InterfacePtr<IGraphicFrameData> graphicFrameDataFlow(selectedFrameRef, UseDefaultIID());
			if (!graphicFrameDataFlow) 
			{
				break;
			}

			result = graphicFrameDataFlow->GetTextContentUID();
			
	}	while(false);
	return(result);
}*/
